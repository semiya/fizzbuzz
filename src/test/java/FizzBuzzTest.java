import com.sun.xml.internal.ws.policy.AssertionSet;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class FizzBuzzTest {

    @Test

    public void should_return_0_when_0() {
        int number = 0;
        assertThat(FizzBuzz.transform(number)).isEqualTo("0");
    }

    @Test
    public void should_return_1_when_1() throws Exception {
        int number = 1;
        assertThat(FizzBuzz.transform(number)).isEqualTo("1");
    }

    @Test
    public void should_return_fizz_when_3() throws Exception {
        int number = 3;
        assertThat(FizzBuzz.transform(number)).isEqualTo("fizz");
    }

    @Test
    public void should_return_fizz_when_6() throws Exception {
        int number = 6;
        assertThat(FizzBuzz.transform(number)).isEqualTo("fizz");
    }

    @Test
    public void should_return_buzz_when_5() throws Exception {
        int number = 5;
        assertThat(FizzBuzz.transform(number)).isEqualTo("buzz");
    }
    @Test
    public void should_return_buzz_when_10() throws Exception {
        int number = 10;
        assertThat(FizzBuzz.transform(number)).isEqualTo("buzz");
    }

    @Test
    public void should_return_fizzbuzz_when_15() throws Exception {
        int number = 15;
        assertThat(FizzBuzz.transform(number)).isEqualTo("fizzbuzz");
    }

    @Test
    public void should_return_fizzbuzz_when_30() throws Exception {
        int number = 30;
        assertThat(FizzBuzz.transform(number)).isEqualTo("fizzbuzz");

    }
}
