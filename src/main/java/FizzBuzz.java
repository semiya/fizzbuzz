import org.assertj.core.internal.Numbers;

public class FizzBuzz {
    public static final int THREE = 3;
    public static final int FIVE = 5;
    public static final String FIZZ = "fizz";
    public static final String BUZZ = "buzz";
    public static final String FIZZBUZZ = "fizzbuzz";


    public static String transform(int number) {
        if (number % THREE == 0 && number % FIVE == 0) {
            return FIZZBUZZ;
        } else if (number % FIVE == 0) {
            return BUZZ;
        } else if (number % THREE == 0) {
            return FIZZ;
        } else {
            return String.valueOf(number);
        }

    }
}
